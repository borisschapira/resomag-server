# ReSoMag-Server

The back-end for [the ReSoMaG game](https://github.com/borisschapira/ReSoMaG), handling scores (for now)

## Development

## Prerequisites

- nodejs[^nvm] (tested with v12.x and v13.x)  
  (_I strongly recommend the use of [nvm](https://github.com/nvm-sh/nvm)_)

## Installation

Clean install the project using `npm ci`. The project is based on ExpressJS and has been bootstraped using [express-generator](https://github.com/expressjs/generator).

## Configuration

The project lies on several environment keys. For the web app to expose itself on the server:

- `PORT`: the port on which the project is listening (default: 3000)
- `IP`: the ip on which the project is exposed (default: 127.0.0.1)

For the web app to communicate with the MySql server:

- `RSMG_HOST`: the database host
- `RSMG_USER`: the database user
- `RSMG_PASS`: the database password
- `RSMG_DB`: the database name

For the web app to be restarted on AlwaysData (don't set if your not using AlwaysData hosting):

- `ALWAYSDATA_API_KEY`

## Deployment

### Prerequisites

- ssh
- rsync
- a mySql / MariaDB database

### via the CLI

The deployment is divided into several phases:

1. **On your server**: implement [the database structure](mysql/dump.sql) in your mySQL/MariaDB database.
2. **From your computer**: execute `npm run deploy` to:
   1. Copy the files to the server via `rsync`  
   (can also be accomplished through `npm run deploy:files`)
   1. Execute `npm install` on the server via `ssh`  
   (can also be accomplished through `npm run deploy:install`)
   1. Call the [AlwaysData REST API](https://www.alwaysdata.com/fr/services/api/) to restart the web server  
   (can also be accomplished through `npm run deploy:restart`)
