var express = require("express");
var router = express.Router();

/* GET scores listing. */
router.get("/", function(req, res) {
  res.locals.connection.query(
    "SELECT score, name from scores ORDER BY score ASC LIMIT 10",
    function(error, results) {
      res.locals.connection.end();
      if (error) {
        res.send(JSON.stringify({ status: 500, error: error, response: null }));
      } else {
        res.send(
          JSON.stringify({ status: 200, error: null, response: results })
        );
      }
    }
  );
});

router.post("/", function(req, res) {
  console.log(req.body);
  if (
    !req.body.hasOwnProperty("score") ||
    !req.body.hasOwnProperty("name") ||
    !Number.isInteger(req.body.score)
  ) {
    res.statusCode = 400;
    return res.send("Error 400: Post syntax incorrect.");
  }

  var post = { score: req.body.score, name: req.body.name };
  res.locals.connection.query("INSERT INTO `scores` SET ?", post, function(
    error,
    results
  ) {
    if (error) {
      res.send(JSON.stringify({ status: 500, error: error, response: null }));
    } else {
      res.locals.connection.query(
        "SELECT score, name from scores ORDER BY score ASC LIMIT 10",
        function(error, results) {
          if (error) {
            res.send(
              JSON.stringify({ status: 500, error: error, response: null })
            );
          } else {
            res.send(
              JSON.stringify({ status: 200, error: null, response: results })
            );
          }
        }
      );
    }
    res.locals.connection.end();
  });
});

module.exports = router;
